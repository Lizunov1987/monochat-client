﻿using MonoChat_client.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace MonoChat_client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HttpClient _httpClient = new();
        public MainWindow()
        {
            InitializeComponent();
            _httpClient.BaseAddress = new Uri("http://localhost:5000");
        }

        private async Task GetAllMessages(DateTime? start = null, DateTime? end = null)
        {
            Messages.Items.Clear();
            var @params = new
            {
                start,
                end
            };
            var json = JsonSerializer.Serialize(@params);
            var answer = await _httpClient.PostAsync("api/getMessages", new StringContent(json, Encoding.UTF8, "application/json"));
            var response = await answer.Content.ReadAsStringAsync();
            var messages = JsonSerializer.Deserialize<Message[]>(response, null);
            foreach (var msg in messages)
            {
                Messages.Items.Add(msg.sentTime.ToString("[dd.MM.yy HH:mm:ss]") + " " + msg.userName + " " + msg.text);
            }
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                MessageSendButton(null, null);
            }
        }

        private async void Messages_Loaded(object sender, RoutedEventArgs e)
        {
            await GetAllMessages();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await GetAllMessages(StartPeriod.SelectedDate, EndPeriod.SelectedDate);
        }

        private async void MessageSendButton(object sender, RoutedEventArgs e)
        {
            var userName = Environment.UserName;
            var content = new
            {
                username = userName,
                text = input.Text
            };
            var json = JsonSerializer.Serialize(content);
            var sendStatus = await _httpClient.PostAsync("api/addMessage", new StringContent(json, Encoding.UTF8, "application/json"));
            var response = await sendStatus.Content.ReadAsStringAsync();
            var msgTime = DateTime.Parse(response);
            Messages.Items.Add(msgTime.ToString("[dd.MM.yy HH:mm:ss]") + " " + userName + " " + input.Text);
            input.Text = string.Empty;
        }
        
    }
}
