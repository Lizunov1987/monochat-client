﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoChat_client.Model
{
    class Message
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string text { get; set; }
        public DateTime sentTime { get; set; }
    }
}
